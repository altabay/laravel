@extends('layouts.app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="panel panel-default">
					<div class="panel-heading">Dashboard</div>

					<div class="panel-body">
						<!-- Modal -->
						<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">Modal title</h4>
									</div>
									<div class="modal-body">
										...
									</div>
									<div class="modal-footer">
										<form method="post" action=""> </form>
										<button type="button" onclick="register()" class="btn btn-primary">Pay</button>
										<button type="button" onclick="cancelReg()" class="btn btn-default" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script>
        $('#myModal').modal('show');

        function register() {
            $.ajax({
                url: "/register/complete",
                method: "post",
                data: {_token: '{{ csrf_token() }}'},
                success: function (result) {
                    window.location = "/home"
                },
                error: function (result) {
                    alert("Some error");
                }
            });
        }

        function cancelReg() {
            $.ajax({
                url: "/register/cancel",
                method: "post",
				data: {_token: '{{ csrf_token() }}'},
                success: function (result) {
                    if(result.status === "redirect"){
                        window.location = "/register"
                    }
                },
                error: function (result) {
                    alert("Some error");
                }
            });
        }
	</script>
@endsection
