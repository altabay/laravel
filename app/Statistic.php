<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Statistic extends Model
{

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'phone', 'email', 'popup_open_number', 'number_payments'
	];
}
