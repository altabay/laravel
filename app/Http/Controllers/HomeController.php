<?php

namespace App\Http\Controllers;

use App\Statistic;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$users = User::all();
		$pops = Statistic::all();
		$numbers = 0;
		$numbersPayment = 0;
		foreach ($pops as $p){
			$numbers += $p->popup_open_number;
			$numbersPayment += $p->number_payments;
		}
        return view('home', ['users' => $users, 'pops' => $pops, 'numbers' => $numbers, 'numbersPayment' => $numbersPayment]);
    }
}
