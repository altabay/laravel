<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/popup', 'Auth\RegisterController@popup')->name('popup');

//Route::post('/register', 'Auth\RegisterController@createCustome')->name('createCustome');
Route::post('/register/complete', 'Auth\RegisterController@createComplete')->name('createComplete');
Route::post('/register/cancel', 'Auth\RegisterController@cancel')->name('cancel');


